package pkg

import (
	"encoding/json"
	"log"
	"os/exec"
	"sync"

	"gitlab.com/tigorlazuardi/seraph-kube/models"
)

func Exec() {
	prefix := "seraph-kube ==>"
	out, err := exec.Command("kubectl", "get", "pods", "-o", "json").Output()
	if err != nil {
		log.Fatal(err)
	}
	pods := models.PodList{}
	err = json.Unmarshal(out, &pods)
	if err != nil {
		log.Fatal(err)
	}

	images := models.FilterImageList(pods)
	wg := sync.WaitGroup{}
	for _, v := range images {
		wg.Add(1)
		go func(v *models.Image) {
			defer wg.Done()
			out, err := exec.Command("skopeo", "inspect", "docker://"+v.Name).Output()
			if err != nil {
				log.Println(prefix, "skopeo failed to inspect:", v.Name)
				return
			}
			digest := models.SkopeoResult{}
			json.Unmarshal(out, &digest)
			if v.SHA256 != digest.Digest {
				log.Println(prefix, "Updating pods with image:", v.Name)
				args := []string{"delete", "pods"}
				for _, s := range v.Pods {
					args = append(args, s)
				}
				out, _ := exec.Command("kubectl", args...).Output()
				log.Println(prefix, string(out))
			}
		}(v)
	}
	wg.Wait()
}
