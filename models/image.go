package models

import "strings"

type ImageWithSHA string

type ImageList map[ImageWithSHA]*Image

type Image struct {
	Name   string
	Pods   []string
	SHA256 string
}

func FilterImageList(pods PodList) ImageList {
	images := make(ImageList)

	// Check all container list
	for _, item := range pods.Items {

		// Check the images
		for _, container := range item.Status.ContainerStatuses {

			// Reduce duplicate images with same SHA256/ImageID
			if value, ok := images[ImageWithSHA(container.ImageID)]; ok {
				for _, cont := range item.Spec.Containers {
					if cont.ImagePullPolicy == "Always" && !contains(value.Pods, item.Metadata.Name) {
						value.Pods = append(value.Pods, item.Metadata.Name)
					}
				}
			} else {
				// Get the Digest SHA256
				s := strings.Split(container.ImageID, "@")
				// Dealing with Possibilty of ImageID is empty
				if len(s) < 2 {
					continue
				}
				images[ImageWithSHA(container.ImageID)] = &Image{
					Name:   container.Image,
					Pods:   []string{item.Metadata.Name},
					SHA256: s[1],
				}
			}
		}
	}
	return images
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

type PodList struct {
	Items []Item `json:"items,omitempty"`
}

type Item struct {
	Metadata struct {
		Name string `json:"name,omitempty"`
	} `json:"metadata,omitempty"`
	Status struct {
		ContainerStatuses []ContainerStatus `json:"containerStatuses,omitempty"`
	} `json:"status,omitempty"`
	Spec struct {
		Containers []Container `json:"containers,omitempty"`
	} `json:"spec,omitempty"`
}

type Container struct {
	ImagePullPolicy string `json:"imagePullPolicy,omitempty"`
}

type ContainerStatus struct {
	Image   string `json:"image,omitempty"`
	ImageID string `json:"imageID,omitempty"`
}

type SkopeoResult struct {
	Digest string `json:"digest,omitempty"`
}
