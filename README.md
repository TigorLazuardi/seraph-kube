seraph-kube

Seraph Kube is an Application to listen to Kubernetes Pods, and check periodically for new images used in said pods.

# Requirements

1. Working and Running kubectl. If Kubernetes is inside a VM (like a Minikube), install this App on that VM.
2. [skopeo](https://github.com/containers/skopeo) installed ***NOT AS A DOCKER IMAGE***. (This app cannot handle what skopeo cannot handle)
3. Pods / Deployments containers have `imagePullPolicy` set to `Always`. The app currently only check for those.
4. For private repositories, make sure skopeo can access them.

# What this App Do?

Seraph-Kube basically checks for images that is used in current (and running) namespace pods, then checks for the digests in the registry using skopeo.

Then it compares the SHA256 of k8s images to the digests, and if there's a difference, will then tell kubectl to kill all pods with imagePullPolicy of Always that uses the image.

# Installation

Make sure `$GOPATH/bin` or `$HOME/go/bin` is in your `$PATH`.

```shell
go get -u gitlab.com/tigorlazuardi/seraph-kube
```

# How to Use

## As Daemon

```shell
seraph-kube daemon --interval 30 # Checks every 30 seconds for new images (Default 5 minutes)
```

## Run Once

```shell
seraph-kube
```