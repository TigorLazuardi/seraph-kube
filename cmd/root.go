package cmd

import (
	"log"
	"os/exec"

	"github.com/spf13/cobra"
	"gitlab.com/tigorlazuardi/seraph-kube/pkg"
)

var rootCmd = &cobra.Command{
	Use:   "seraph-kube",
	Short: "seraph-kube is a tool to look at pods whose image tags had outdated image and then update them.",
	Long: `seraph-kube is a tool to look at pods whose image tags had outdated image and then update them.
It works by looking at the images SHA256 currently used by Kubernetes,
and then compare them to the one currently on the registry.

If it doesn't match, seraph-kube will then ask Kubernetes to kill all the pods that had different SHA256.

By default, seraph-kube will only kill pods that have imagePullPolicy set to always`,
	Run: func(cmd *cobra.Command, args []string) {
		pkg.Exec()
	},
}

// Exec runs the cobra command parser
func Exec() {
	validateExecutables("kubectl", "skopeo")
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func validateExecutables(programs ...string) {
	for _, p := range programs {
		_, err := exec.LookPath(p)
		if err != nil {
			log.Fatal(err)
		}
	}
}
