package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tigorlazuardi/seraph-kube/pkg"
)

var daemonCmd = &cobra.Command{
	Use:     "daemon",
	Short:   "Runs the executable as background task",
	Long:    `Runs the executable as background task.`,
	Example: "seraph-kube daemon --port 4444",
	Run: func(cmd *cobra.Command, args []string) {
		prefix := "seraph-kube ==>"

		viper.SetEnvPrefix("SERAPHKUBE_")
		viper.RegisterAlias("INTERVAL", "interval")
		viper.AutomaticEnv()
		viper.BindPFlags(cmd.Flags())
		viper.SetDefault("interval", 300)

		interval := time.Duration(viper.GetUint64("interval")) * time.Second
		ticker := time.NewTicker(interval)
		for {
			log.Println(prefix, "Next Schedule:", time.Now().Add(interval))
			pkg.Exec()
			<-ticker.C
		}
	},
}

func init() {
	var interval uint64
	daemonCmd.Flags().Uint64VarP(&interval, "interval", "i", 0, "Customize the time needed to pass before seraph-kube do another check.")
	rootCmd.AddCommand(daemonCmd)
}
