package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var systemdCmd = &cobra.Command{
	Use:   "systemd",
	Short: "systemd installs this app as systemd service",
	Long: `systemd installs this app as systemd service.

Installing this as a service makes it always start after login.

This app requires root unless given --user flag because by default it installs to system dir of systemd
where otherwise it will be installed on $HOME/.config/systemd/user/

What this app does:
It writes systemd config and install, then run daemon-reload, and then enable/start the service.

Warning! This service may be blocked if SELinux is set to Enforcing.
`,
	Example: "seraph-kube systemd --interval 30 --user",
	Run: func(cmd *cobra.Command, args []string) {
		const prefix = "seraph-kube ==>"
		viper.BindPFlags(cmd.Flags())

		const filename = "seraph-kube.service"

		interval := viper.GetUint64("interval")
		user := viper.GetBool("user")

		if !user && os.Geteuid() != 0 {
			fmt.Println(prefix, "Installing to system requires privileged permissions.")
			fmt.Println(prefix, "Run 'seraph-kube systemd --help' for help")
			os.Exit(1)
		}

		var target string
		var base string
		var home = os.Getenv("HOME")
		var goPath = os.Getenv("GOPATH")

		if goPath == "" {
			base = home + "/go"
		} else {
			base = goPath
		}

		if user {
			target = home + "/.config/systemd/user/"
		} else {
			target = "/usr/lib/systemd/system/"
		}

		servo := fmt.Sprintf(`[Unit]
Description=Seraph Kube Service
ConditionPathExists=%s/bin/seraph-kube
After=network.target

[Service]
Type=simple
ExecStart=%s/bin/seraph-kube daemon --interval %d
Restart=always
RestartSec=8

[Install]
WantedBy=multi-user.target
`,
			base,
			base,
			interval,
		)

		content := []byte(servo)
		err := ioutil.WriteFile(target+filename, content, 0644)
		if err != nil {
			fmt.Println(prefix, err)
			os.Exit(1)
		}

		if user {
			_, err := exec.Command("systemctl", "--user", "daemon-reload").Output()
			if err != nil {
				fmt.Println(prefix, err)
				os.Exit(1)
			}
			out, err := exec.Command("systemctl", "--user", "--now", "enable", "seraph-kube").Output()
			if err != nil {
				fmt.Println(prefix, err)
				os.Exit(1)
			} else {
				fmt.Println(prefix, string(out))
			}
		} else {
			_, err := exec.Command("systemctl", "daemon-reload").Output()
			if err != nil {
				fmt.Println(prefix, err)
				os.Exit(1)
			}
			out, err := exec.Command("systemctl", "--now", "enable", "seraph-kube").Output()
			if err != nil {
				fmt.Println(prefix, err)
				os.Exit(1)
			} else {
				fmt.Println(prefix, string(out))
			}
		}

		log.Println(prefix, "Service installed at:", target+filename)
	},
}

func init() {
	var interval uint64
	var user bool

	systemdCmd.Flags().Uint64VarP(&interval, "interval", "i", 30, "Specifies the interval between checkings")
	systemdCmd.Flags().BoolVar(&user, "user", false, "Install as current user")

	rootCmd.AddCommand(systemdCmd)
}
